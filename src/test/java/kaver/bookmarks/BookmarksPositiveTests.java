package kaver.bookmarks;

import io.qameta.allure.*;
import kaver.BaseTest;
import org.junit.jupiter.api.*;

@Epic("UI")
@Tag("Позитивный")
@Feature("Избранное")
public class BookmarksPositiveTests extends BaseTest {
    @Test
    @DisplayName("Добавление места в избранное")
    @Severity(value = SeverityLevel.NORMAL)
    void addPlaceToBookmarks() {
        placesPage = mainPage.navbarElement.clickPlacesBtn();
        placeCategoryPage = placesPage.clickArtSpacesBtn();
        String placeTitle = placeCategoryPage.getFirstPlaceTitle();
        placeCategoryPage = placeCategoryPage.addFirstPlaceToBookmarks()
            .feedbackWidget.clickYesBtn();
        bookmarksPage = placeCategoryPage.navbarElement.clickBookmarksBtn();
        placesBookmarksPage = bookmarksPage.clickPlacesBookmarksBtn();

        Assertions.assertTrue(placesBookmarksPage.checkFirstPlaceTitleEqualsValue(placeTitle));
    }

    @Test
    @DisplayName("Добавление события в избранное")
    @Severity(value = SeverityLevel.NORMAL)
    void addEventToBookmarks() {
        selectionPage = mainPage.openFreeSelectionPage();
        String eventTitleTitle = selectionPage.getFirstEventTitle();
        selectionPage = selectionPage.addFirstEventToBookmarkBtn()
            .feedbackWidget.clickYesBtn();
        bookmarksPage = selectionPage.navbarElement.clickBookmarksBtn();
        eventsBookmarksPage = bookmarksPage.clickEventsBookmarksBtn();

        Assertions.assertTrue(eventsBookmarksPage.checkFirstEventTitleEqualsValue(eventTitleTitle));
    }

    @AfterEach
    @Step("Убрать добавленное из избранное")
    void removeFromFavourites() {
        if (eventsBookmarksPage != null) {
            eventsBookmarksPage.removeFirstFromFavourites();
        }
        else if (placesBookmarksPage != null){
            placesBookmarksPage.removeFirstFromFavourites();
        }
    }
}
