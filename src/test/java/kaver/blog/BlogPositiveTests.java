package kaver.blog;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import kaver.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Epic("UI")
@Feature("Блог")
@Tag("Позитивный")
public class BlogPositiveTests extends BaseTest {
    @Test
    @DisplayName("Открытие подробной страницы статьи")
    @Severity(value = SeverityLevel.MINOR)
    void openDetailArticlePage() {
        blogPage = mainPage.clickBlogBtn();
        String title = blogPage.getFirstArticleTitle();
        articlePage = blogPage.clickFirstArticleTitle();

        Assertions.assertTrue(articlePage.checkTitleEquals(title));
    }
}
