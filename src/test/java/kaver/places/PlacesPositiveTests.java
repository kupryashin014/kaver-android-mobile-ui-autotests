package kaver.places;

import io.qameta.allure.*;
import kaver.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Epic("UI")
@Tag("Позитивный")
@Feature("Каталог мест")
public class PlacesPositiveTests extends BaseTest {
    @Test
    @DisplayName("Поиск мест по названию")
    @Severity(value = SeverityLevel.NORMAL)
    void searchPlacesByName() {
        String searchString = "Про";

        placesPage = mainPage.navbarElement.clickPlacesBtn();
        searchPage = placesPage.clickSearchBtn();
        searchResultsPage = searchPage.search(searchString);

        Assertions.assertTrue(searchResultsPage.checkResultsTitlesContain(searchString));
    }
}
