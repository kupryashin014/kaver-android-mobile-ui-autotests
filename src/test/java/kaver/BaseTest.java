package kaver;

import io.qameta.allure.Step;
import kaver.allure.TestExecutionListenerExtension;
import kaver.config.Settings;
import kaver.pages.auth.AuthByPhonePage;
import kaver.pages.auth.AuthPage;
import kaver.pages.auth.ChooseAccountPage;
import kaver.pages.blog.ArticlePage;
import kaver.pages.blog.BlogPage;
import kaver.pages.bookmarks.BookmarksPage;
import kaver.pages.bookmarks.EventsBookmarksPage;
import kaver.pages.bookmarks.PlacesBookmarksPage;
import kaver.pages.main.MainPage;
import kaver.pages.onboarding.*;
import kaver.pages.places.PlaceCategoryPage;
import kaver.pages.places.PlacesPage;
import kaver.pages.search.SearchPage;
import kaver.pages.search.SearchResultsPage;
import kaver.pages.selection.SelectionPage;
import kaver.widgets.InfoWidget;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(TestExecutionListenerExtension.class)
public class BaseTest extends Settings {
    public ArticlePage articlePage;
    public AuthByPhonePage authByPhonePage;
    public AuthPage authPage;
    public BirthdayDateSelectionPage birthdayDateSelectionPage;
    public BlogPage blogPage;
    public BookmarksPage bookmarksPage;
    public CategorySelectionPage categorySelectionPage;
    public ChooseAccountPage chooseAccountPage;
    public CitySelectionPage citySelectionPage = new CitySelectionPage();
    public EventsBookmarksPage eventsBookmarksPage = null;
    public GoalsPage goalsPage;
    public InfoPage infoPage;
    public InfoWidget<MainPage> infoWidget;
    public MainPage mainPage;
    public OnboardingPage onboardingPage;
    public PlaceCategoryPage placeCategoryPage;
    public PlacesBookmarksPage placesBookmarksPage = null;
    public PlacesPage placesPage;
    public SearchPage searchPage;
    public SearchResultsPage searchResultsPage;
    public SelectionPage selectionPage;

    @BeforeEach
    void setUp() {
        initDriver();
    }

    @Step("Пройти онбординг")
    @BeforeEach
    void doOnboarding() {
        goalsPage = citySelectionPage.clickSaintPetersburgBtn().clickNextBtn();
        birthdayDateSelectionPage = goalsPage.clickSeeBillboardsBtn();
        onboardingPage = birthdayDateSelectionPage.setAdult();
        authPage = onboardingPage.skipInfo();
        chooseAccountPage = authPage.clickLoginByPhoneNumberBtn();
        authByPhonePage = chooseAccountPage.deleteExistingAndAddNewAccount();
        categorySelectionPage = authByPhonePage.authByPhone();
        infoPage = categorySelectionPage.chooseCategories();
        mainPage = infoPage.clickNextBtn();
        infoWidget = new InfoWidget<>(mainPage);
        mainPage = infoWidget.clickCloseInfoBtn();
    }

    @AfterEach
    void removeApp() {
        getDriver().removeApp("info.goodline.events");
        getDriver().quit();
    }

}
