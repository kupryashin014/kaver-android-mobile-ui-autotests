package kaver.allure;

import io.qameta.allure.Attachment;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.LifecycleMethodExecutionExceptionHandler;
import org.openqa.selenium.OutputType;

import java.util.Base64;

import static kaver.config.Settings.getDriver;


public class TestExecutionListenerExtension implements AfterTestExecutionCallback,
        BeforeTestExecutionCallback, LifecycleMethodExecutionExceptionHandler {
    public TestExecutionListenerExtension() {

    }

    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) {
        getDriver().startRecordingScreen();
    }

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) {
        processException(extensionContext);
    }

    @Attachment(value = "Dom", type = "text/xml")
    private String appendDom() {
        return getDriver().getPageSource();
    }

    @Attachment(value="Видеозапись", type="video/mp4", fileExtension = ".mp4")
    private byte[] appendScreenRecord(){
        return Base64.getDecoder().decode(getDriver().stopRecordingScreen());
    }

    @Attachment(value = "Скриншот", type = "image/png")
    private byte[] appendScreenshot(){
        return getDriver().getScreenshotAs(OutputType.BYTES);
    }

    private void processException(ExtensionContext extensionContext) {
        if (extensionContext.getExecutionException().isPresent()){
            appendDom();
        }
        appendScreenRecord();
        appendScreenshot();
    }

    public void handleBeforeAllMethodExecutionException(ExtensionContext extensionContext, Throwable throwable)
            throws Throwable {
        this.processException(extensionContext);
        throw throwable;
    }

    public void handleBeforeEachMethodExecutionException(ExtensionContext extensionContext, Throwable throwable)
            throws Throwable {
        this.processException(extensionContext);
        throw throwable;
    }

    public void handleAfterEachMethodExecutionException(ExtensionContext extensionContext, Throwable throwable)
            throws Throwable {
        this.processException(extensionContext);
        throw throwable;
    }

    public void handleAfterAllMethodExecutionException(ExtensionContext extensionContext, Throwable throwable)
            throws Throwable {
        this.processException(extensionContext);
        throw throwable;
    }
}
