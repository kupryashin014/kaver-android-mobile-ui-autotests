package kaver.constants;

public interface Constants extends Secrets {
    String
        APPIUM_APP  = "src/test/resources/kaver/apk/" + APP_NAME,
        DEVICE_NAME = "Pixel 3a";
}
