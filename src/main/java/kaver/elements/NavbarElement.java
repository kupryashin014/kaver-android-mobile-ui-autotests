package kaver.elements;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.pages.bookmarks.BookmarksPage;
import kaver.pages.places.PlacesPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.interactable;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class NavbarElement {
    private final SelenideElement
        bookmarksBtn = $(By.id("favorite")),
        placesBtn    = $(By.id("map"));

    @Step("Перейти в раздел места")
    public PlacesPage clickPlacesBtn() {
        placesBtn.shouldBe(interactable).click();

        return new PlacesPage();
    }

    @Step("Перейти в раздел Избранное")
    public BookmarksPage clickBookmarksBtn() {
        bookmarksBtn.click();

        return new BookmarksPage();
    }
}
