package kaver.config;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.remote.DesiredCapabilities;

public abstract class DriverBuilder <T extends DriverBuilder> {
    DesiredCapabilities capabilities;
    protected AppiumDriverLocalService service;

    public T setDeviceName(String deviceName) {
        capabilities.setCapability("deviceName", deviceName);

        return (T) this;
    }

    public T noReset() {
        capabilities.setCapability("noReset", true);

        return (T) this;
    }

    public T fullReset() {
        capabilities.setCapability("fullReset", true);

        return (T) this;
    }

    public T setCapability(String capability, String value) {
        capabilities.setCapability(capability, value);

        return (T) this;
    }
}
