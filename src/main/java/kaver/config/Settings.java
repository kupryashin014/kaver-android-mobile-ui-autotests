package kaver.config;

import com.codeborne.selenide.Configuration;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AutomationName;
import kaver.constants.Constants;
import org.openqa.selenium.Platform;

public class Settings implements Constants {
    public static AndroidDriver getDriver() {
        return driver;
    }

    private static AndroidDriver driver;

    public static void initDriver() {
        driver = new AndroidDriverBuilder()
            .setDevice(DEVICE_NAME)
            .setPlatform(Platform.ANDROID)
            .setAutomation(AutomationName.ANDROID_UIAUTOMATOR2)
            .setApp(APPIUM_APP)
            .autoGrantPermissions()
            .disableIdLocatorAutocompletion()
            .init();

        Configuration.timeout = 15000;
    }

}
