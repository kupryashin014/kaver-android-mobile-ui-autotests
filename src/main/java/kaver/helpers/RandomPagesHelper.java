package kaver.helpers;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebElementCondition;
import com.codeborne.selenide.ex.ElementNotFound;

import java.time.Duration;

public class RandomPagesHelper {
    public static Boolean isElementExist(SelenideElement element, WebElementCondition condition) {
        try {
            return element.shouldBe(condition, Duration.ofSeconds(1)).isDisplayed();
        }
        catch (ElementNotFound e) {
            return false;
        }
    }

}
