package kaver.pages.search;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.appium.SelenideAppium.$;
import static kaver.conditions.CustomCondition.customCondition;

public class SearchResultsPage {
    private final ElementsCollection searchResultsTitles = $$(By.id("place_content"));
    private final SelenideElement eventsList = $(By.id("events_list"));

    @Step("Все найденные места содержат {searchRequest} в названии?")
    public boolean checkResultsTitlesContain(String searchRequest) {
        eventsList.shouldBe(exist, Duration.ofSeconds(30));

        return searchResultsTitles.shouldBe(customCondition)
            .asDynamicIterable()
            .stream()
            .allMatch(element -> element.text().contains(searchRequest));
    }
}
