package kaver.pages.search;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;

public class SearchPage {
    private final SelenideElement searchField = $(By.id("input"));

    @Step("Ввести {searchString} в поле для поиска")
    public SearchResultsPage search(String searchString){
        searchField.sendKeys(searchString);

        return new SearchResultsPage();
    }
}
