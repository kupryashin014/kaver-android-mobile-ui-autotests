package kaver.pages.places;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import kaver.elements.NavbarElement;
import kaver.widgets.FeedbackWidget;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;

public class PlaceCategoryPage {
    public FeedbackWidget<PlaceCategoryPage> feedbackWidget;

    public NavbarElement navbarElement = new NavbarElement();

    PlaceCategoryPage() {
        feedbackWidget = new FeedbackWidget<>(this);
    }

    private final SelenideElement
        firstPlaceAddToBookmarkBtn = $(By.id("favorite_button")),
        firstPlaceTitle            = $(By.id("place_content"));

    @Step("Считать название первого места")
    @Attachment(value = "Название")
    public String getFirstPlaceTitle() {
        return firstPlaceTitle.text();
    }

    @Step("Добавить первое место в избранное")
    public PlaceCategoryPage addFirstPlaceToBookmarks() {
        firstPlaceAddToBookmarkBtn.click();

        return this;
    }
}
