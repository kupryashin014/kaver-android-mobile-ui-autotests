package kaver.pages.bookmarks;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class PlacesBookmarksPage {
    private final SelenideElement
        firstPlaceAddToBookmarkBtn = $(By.id("favorite_button")),
        firstPlaceTitle            = $(By.id("place_content"));

    @Step("Название первого места совпадает с {title}?")
    public boolean checkFirstPlaceTitleEqualsValue(String title) {
        return firstPlaceTitle.text().equals(title);
    }

    @Step("Убрать место из избранного")
    public void removeFirstFromFavourites() {
        firstPlaceAddToBookmarkBtn.click();
        firstPlaceAddToBookmarkBtn.shouldNotBe(exist);
    }
}
