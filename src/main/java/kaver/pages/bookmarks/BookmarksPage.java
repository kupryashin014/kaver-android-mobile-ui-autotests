package kaver.pages.bookmarks;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class BookmarksPage {
    private final SelenideElement
        eventsBookmarksBtn = $x("(//android.view.ViewGroup[@resource-id=\"events\"])[2]"),
        placesBookmarksBtn = $x("(//android.view.ViewGroup[@resource-id=\"places\"])[2]");

    @Step("Перейти в избранные события")
    public EventsBookmarksPage clickEventsBookmarksBtn() {
        eventsBookmarksBtn.click();

        return new EventsBookmarksPage();
    }

    @Step("Перейти в избранные места")
    public PlacesBookmarksPage clickPlacesBookmarksBtn() {
        placesBookmarksBtn.click();

        return new PlacesBookmarksPage();
    }
}
