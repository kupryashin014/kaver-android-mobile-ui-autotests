package kaver.pages.selection;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import kaver.elements.NavbarElement;
import kaver.widgets.FeedbackWidget;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class SelectionPage {
    public NavbarElement navbarElement = new NavbarElement();
    public FeedbackWidget<SelectionPage> feedbackWidget;

    public SelectionPage() {
        feedbackWidget = new FeedbackWidget<>(this);
    }

    private final SelenideElement
        firstEventAddToBookmarkBtn = $(By.id("favorite_button")),
        firstEventTitle            = $(By.id("event_content"));

    private final ElementsCollection eventsCards = $$x("//android.view.ViewGroup" +
            "[@resource-id=\"event\"]//android.view.ViewGroup[@resource-id=\"info\"]/" +
            "following-sibling::android.widget.TextView");

    @Step("Все события бесплатные?")
    public boolean checkCostOfAllEvents(String cost){
        return eventsCards.asDynamicIterable().stream()
            .allMatch(element -> element.text().equals(cost));
    }

    @Step("Считать название первого события")
    @Attachment(value = "Название")
    public String getFirstEventTitle() {
        return firstEventTitle.text();
    }

    @Step("Добавить первое событие в избранное")
    public SelectionPage addFirstEventToBookmarkBtn() {
        firstEventAddToBookmarkBtn.click();

        return this;
    }
}
