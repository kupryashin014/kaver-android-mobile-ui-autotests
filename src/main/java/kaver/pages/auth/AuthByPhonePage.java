package kaver.pages.auth;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.pages.onboarding.CategorySelectionPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;
import static kaver.constants.Secrets.CONFIRMATION_CODE;
import static kaver.constants.Secrets.PHONE_NUMBER;

public class AuthByPhonePage {
    private final SelenideElement
        authoriseBtn     = $(By.id("info.goodline.events:id/auth_passport_btn_continue")),
        codeInput        = $(By.id("info.goodline.events:id/auth_passport_et_code")),
        phoneNumberInput = $(By.id("info.goodline.events:id/auth_passport_et_phone_number")),
        sendCodeBtn      = $(By.id("info.goodline.events:id/auth_passport_btn_phone_number"));

    @Step("Ввести номер телефона {phoneNumber}")
    private AuthByPhonePage enterPhoneNumber(String phoneNumber) {
        phoneNumberInput.sendKeys(phoneNumber);

        return this;
    }

    @Step("Авторизоваться по номеру телефона")
    public CategorySelectionPage authByPhone() {
        return this.enterPhoneNumber(PHONE_NUMBER)
            .clickSendCodeBtn()
            .enterConfirmationCode(CONFIRMATION_CODE)
            .clickAuthoriseBtn();
    }

    @Step("Нажать отправить код")
    private AuthByPhonePage clickSendCodeBtn() {
        sendCodeBtn.click();

        return this;
    }

    @Step("Ввести код подтверждения {confirmationCode}")
    private AuthByPhonePage enterConfirmationCode(String confirmationCode) {
        codeInput.sendKeys(confirmationCode);

        return this;
    }


    @Step("Нажать авторизоваться")
    private CategorySelectionPage clickAuthoriseBtn() {
        authoriseBtn.click();

        return new CategorySelectionPage();
    }
}
