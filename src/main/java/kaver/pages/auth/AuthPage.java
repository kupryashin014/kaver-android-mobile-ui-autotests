package kaver.pages.auth;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class AuthPage {
    private final SelenideElement loginByPhoneNumberBtn = $x("//android.widget.TextView[@text = " +
            "\"По номеру телефона\"]");

    @Step("Выбрать авторизацию по телефону")
    public ChooseAccountPage clickLoginByPhoneNumberBtn() {
        loginByPhoneNumberBtn.click();

        return new ChooseAccountPage();
    }
}
