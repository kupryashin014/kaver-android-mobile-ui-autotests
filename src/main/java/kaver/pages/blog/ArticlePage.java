package kaver.pages.blog;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class ArticlePage {
    private final SelenideElement title = $x("//android.view.ViewGroup[@resource-id=\"author\"]" +
            "/following-sibling::android.widget.TextView[1]");

    @Step("Статья называется {searchRequest}?")
    public boolean checkTitleEquals(String searchRequest) {
        return title.text().equals(searchRequest);
    }
}
