package kaver.pages.onboarding;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;
import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class CategorySelectionPage {
    private final SelenideElement
        concertCategoryBtn = $x("//android.widget.TextView[@resource-id = \"text\" and " +
            "@text = \"Концерт\"]"),
        lectureCategoryBtn = $x("//android.widget.TextView[@resource-id = \"text\" and " +
                "@text = \"Лекции\"]"),
        next4Btn           = $(By.id("next_4")),
        standUpCategoryBtn = $x("//android.widget.TextView[@resource-id = \"text\" and " +
                "@text = \"Stand up\"]");

    @Step("Выбрать категорию StandUp")
    private CategorySelectionPage clickStandUpBtn() {
        standUpCategoryBtn.click();

        return this;
    }

    @Step("Выбрать категорию концерт")
    private CategorySelectionPage clickConcertBtn() {
        concertCategoryBtn.click();

        return this;
    }

    @Step("Выбрать категорию лекции")
    private CategorySelectionPage clickLecturesBtn() {
        lectureCategoryBtn.click();

        return this;
    }

    @Step("Нажать далее")
    private InfoPage clickNextBtn() {
        next4Btn.click();

        return new InfoPage();
    }

    @Step("Выбрать категории")
    public InfoPage chooseCategories() {
        return this.clickStandUpBtn()
            .clickConcertBtn()
            .clickLecturesBtn()
            .clickNextBtn();
    }
}
