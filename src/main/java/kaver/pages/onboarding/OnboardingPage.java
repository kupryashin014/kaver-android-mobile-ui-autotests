package kaver.pages.onboarding;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.constants.Secrets;
import kaver.pages.auth.AuthPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;

public class OnboardingPage implements Secrets {
    private final SelenideElement
        next0Btn = $(By.id("next_0")),
        next1Btn = $(By.id("next_1")),
        next2Btn = $(By.id("next_2")),
        next3Btn = $(By.id("next_3"));

    @Step("Пропустить баннеры")
    public AuthPage skipInfo() {
        next0Btn.click();
        next1Btn.click();
        next2Btn.click();
        next3Btn.click();

        return new AuthPage();
    }
}
