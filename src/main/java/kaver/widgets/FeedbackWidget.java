package kaver.widgets;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class FeedbackWidget<T> {
    private final T parentPage;

    private final SelenideElement yesBtn = $x("//android.widget.TextView[@text=\"Да\"]");

    public FeedbackWidget(T parentPage){
        this.parentPage = parentPage;
    }

    @Step("Закрыть виджет обратной связи")
    public T clickYesBtn() {
        yesBtn.click();

        return parentPage;
    }
}
