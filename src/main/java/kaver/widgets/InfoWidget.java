package kaver.widgets;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;

public class InfoWidget<T> {
    private final T parentPage;

    private final SelenideElement closeInfoBtn = $(By.id("close"));

    public InfoWidget(T parentPage){
        this.parentPage = parentPage;
    }

    @Step("Закрыть информационный виджет")
    public T clickCloseInfoBtn() {
        closeInfoBtn.click();

        return parentPage;
    }
}
